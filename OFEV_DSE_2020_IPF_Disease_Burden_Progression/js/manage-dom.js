$(function () {

    $(document).ready(function () {

        $('#references').click(function () {
            $('.popup-mask').toggleClass('show');
            $('.popup').toggleClass('visible');
            $(this).toggleClass('orange')
        });

        $('.popup-mask').click(function () {
            closePopup();
        });

        $('.btn-close').click(function () {
            closePopup();
        });

        $('.btn-1').click(function () {
            clearTab2Row();
            $('.row.flow').addClass('slow');
        });

        $('.btn-2').click(function () {
            clearTab2Row();
            $('.tab-2 .row.flow').addClass('moderate');
        });

        $('.btn-2').click(function () {
            clearTab2Row();
            $('.tab-2 .row.flow').addClass('moderate');
        });

        $('.btn-3').click(function () {
            clearTab2Row();
            $('.tab-2 .row.flow').addClass('rapid');
        });


        function clearTab2Row() {
            $('.row.flow').removeClass('slow');
            $('.row.flow').removeClass('moderate');
            $('.row.flow').removeClass('rapid');
        }

        setHeaderButtonsWidth();
        setFotterSectionMargin();
    });

    $(window).on('resize', function(){
        setHeaderButtonsWidth();
        setFotterSectionMargin();
    });
});

function closePopup() {
    $('.popup-mask').removeClass('show');
    $('.popup').removeClass('visible');
    $('#references').removeClass('orange');
    $('#footnotes').removeClass('orange');
}

function setHeaderButtonsWidth() {
    let slideButtonWidth = ($(window).width() - 237) / 5;
   $('#header .button-container .about-ipf').css('width', slideButtonWidth + 'px')
    $('#header .button-container .disease-burden').css('width', slideButtonWidth + 'px')
    $('#header .button-container .diagnosis').css('width', slideButtonWidth + 'px')
    $('#header .button-container .management').css('width', slideButtonWidth + 'px')
    $('#header .button-container .paient-profiles').css('width', slideButtonWidth + 'px')
}

function setFotterSectionMargin() {
    let rightButtonContainerMarginLeft = $(window).width() - $('.button-container.left').width() - $('.button-container.right').width();
    $('.button-container.right').css('margin-left', rightButtonContainerMarginLeft + 'px')
}