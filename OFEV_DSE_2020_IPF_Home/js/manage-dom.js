$(function () {

    $(document).ready(function () {

        $('#references').click(function () {
            if ($(this).hasClass('orange')) {
                closePopup();
            } else {
                closePopup();
                $('.popup-mask').toggleClass('show');
                $('#references-popup').toggleClass('visible');
                $(this).toggleClass('orange');
            }
        });

        $('#footnotes').click(function () {
            if ($(this).hasClass('orange')) {
                closePopup();
            } else {
                closePopup();
                $('.popup-mask').toggleClass('show');
                $('#footnotes-popup').toggleClass('visible');
                $(this).toggleClass('orange');
            }
        });

        $('.popup-mask').click(function () {
            closePopup();
        });

        $('.btn-close').click(function () {
            closePopup();
        });

        setHeaderButtonsWidth();
        setFotterSectionMargin();
    });

    $(window).on('resize', function() { 
        setHeaderButtonsWidth();
        setFotterSectionMargin();
    });
});

function closePopup() {
    $('.popup-mask').removeClass('show');
    $('.popup').removeClass('visible');
    $('#references').removeClass('orange');
    $('#footnotes').removeClass('orange');
}

function setHeaderButtonsWidth() {
    let slideButtonWidth = ($(window).width() - 237) / 5;
   $('#header .button-container .about-ipf').css('width', slideButtonWidth + 'px')
    $('#header .button-container .disease-burden').css('width', slideButtonWidth + 'px')
    $('#header .button-container .diagnosis').css('width', slideButtonWidth + 'px')
    $('#header .button-container .management').css('width', slideButtonWidth + 'px')
    $('#header .button-container .paient-profiles').css('width', slideButtonWidth + 'px')
}

function setFotterSectionMargin() {
    let rightButtonContainerMarginLeft = $(window).width() - $('.button-container.left').width() - $('.button-container.right').width();
    $('.button-container.right').css('margin-left', rightButtonContainerMarginLeft + 'px')
}